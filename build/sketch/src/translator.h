#line 1 "/home/cburn/repos/BotRoss/src/translator.h"
#include "Arduino.h"
#ifndef TRANSLATOR
#define TRANSLATOR


class Translator {
    public:
        Translator();
        void serialUpdateStatus() {
            Serial.println("in status");
        }

        void readSerial() {
            if(!Serial.available()) return;
            String newCommand = Serial.readString();

            if (newCommand.equals("STATUS")){
                serialUpdateStatus();
                return;
            }

        }

};
#endif
