from inputs import get_gamepad
import subprocess
import time
import math
import threading
import serial


class XboxToBotRoss(object):
    MAX_TRIG_VAL = math.pow(2, 8)
    MAX_JOY_VAL = math.pow(2, 15)

    def __init__(self):
        self.LeftJoystickY = 0
        self.LeftJoystickX = 0
        self.RightJoystickY = 0
        self.RightJoystickX = 0
        self.LeftTrigger = 0
        self.RightTrigger = 0
        self.LeftBumper = 0
        self.RightBumper = 0
        self.A = 0
        self.X = 0
        self.Y = 0
        self.B = 0
        self.LeftThumb = 0
        self.RightThumb = 0
        self.Back = 0
        self.Start = 0
        self.LeftDPad = 0
        self.RightDPad = 0
        self.UpDPad = 0
        self.DownDPad = 0
        self.fastestRate = 5000
        self.deadZonePercent = 0.15

        self._monitor_thread = threading.Thread(
            target=self._monitor_controller, args=()
        )
        self._monitor_thread.daemon = True
        self._monitor_thread.start()

    def read(self):
        lx = self.LeftJoystickX if abs(self.LeftJoystickX) > self.deadZonePercent else 0
        ly = (
            -self.LeftJoystickY if abs(self.LeftJoystickY) > self.deadZonePercent else 0
        )
        rx = (
            self.RightJoystickX
            if abs(self.RightJoystickX) > self.deadZonePercent
            else 0
        )
        ry = (
            self.RightJoystickY
            if abs(self.RightJoystickY) > self.deadZonePercent
            else 0
        )

        lb = -self.LeftBumper
        rb = self.RightBumper
        return [lx, ly, rx, ry, lb, rb]

    def _monitor_controller(self):
        while True:
            events = get_gamepad()
            for event in events:
                if event.code == "ABS_Y":
                    self.LeftJoystickY = (
                        event.state / XboxToBotRoss.MAX_JOY_VAL
                    )  # normalize between -1 and 1
                elif event.code == "ABS_X":
                    self.LeftJoystickX = (
                        event.state / XboxToBotRoss.MAX_JOY_VAL
                    )  # normalize between -1 and 1
                elif event.code == "ABS_RY":
                    self.RightJoystickY = (
                        event.state / XboxToBotRoss.MAX_JOY_VAL
                    )  # normalize between -1 and 1
                elif event.code == "ABS_RX":
                    self.RightJoystickX = (
                        event.state / XboxToBotRoss.MAX_JOY_VAL
                    )  # normalize between -1 and 1
                elif event.code == "ABS_Z":
                    self.LeftTrigger = (
                        event.state / XboxToBotRoss.MAX_TRIG_VAL
                    )  # normalize between 0 and 1
                elif event.code == "ABS_RZ":
                    self.RightTrigger = (
                        event.state / XboxToBotRoss.MAX_TRIG_VAL
                    )  # normalize between 0 and 1
                elif event.code == "BTN_TL":
                    self.LeftBumper = event.state
                elif event.code == "BTN_TR":
                    self.RightBumper = event.state
                elif event.code == "BTN_SOUTH":
                    self.A = event.state
                elif event.code == "BTN_NORTH":
                    self.X = event.state
                elif event.code == "BTN_WEST":
                    self.Y = event.state
                elif event.code == "BTN_EAST":
                    self.B = event.state
                elif event.code == "BTN_THUMBL":
                    self.LeftThumb = event.state
                elif event.code == "BTN_THUMBR":
                    self.RightThumb = event.state
                elif event.code == "BTN_SELECT":
                    self.Back = event.state
                elif event.code == "BTN_START":
                    self.Start = event.state
                elif event.code == "BTN_TRIGGER_HAPPY1":
                    self.LeftDPad = event.state
                elif event.code == "BTN_TRIGGER_HAPPY2":
                    self.RightDPad = event.state
                elif event.code == "BTN_TRIGGER_HAPPY3":
                    self.UpDPad = event.state
                elif event.code == "BTN_TRIGGER_HAPPY4":
                    self.DownDPad = event.state

    def processToSerial(self, values):
        lx, ly, rx, ry, lb, rb = values
        da = lx + ly
        db = lx - ly
        axis0 = 0 if da == 0 else int(math.floor(self.fastestRate / da))
        axis1 = 0 if da == 0 else int(math.floor(self.fastestRate / db))
        print((da, db))
        print((axis0, axis1))
        numXSteps = 0 if axis0 == 0 else 100
        numYSteps = 0 if axis1 == 0 else 100
        aStr = f"R 0 {numXSteps} {axis0}"
        print(aStr)
        bStr = f"R 1 {numYSteps} {axis1}"
        print(bStr)
        e0 = f'echo "{aStr}">/dev/ttyACM0'
        e1 = f'echo "{bStr}">/dev/ttyACM0'
        print(e0)

        zDelay = (lb + rb) * 20000
        zSteps = abs(lb + rb) * 10
        zStr = f"R 2 {zSteps} {zDelay}"
        e2 = f'echo "{zStr}">/dev/ttyACM0'

        rotSpeed = 0 if rx == 0 else int(math.floor(5000 / rx))
        rotServo = f"R 3 {1} {rotSpeed}"
        rotServoEcho = f'echo "{rotServo}">/dev/ttyACM0'

        pitchSpeed = 0 if ry == 0 else int(math.floor(5000 / ry))
        pitchServo = f"R 4 {1} {pitchSpeed}"
        pitchServoEcho = f'echo "{pitchServo}">/dev/ttyACM0'

        print(rotServoEcho)
        print(pitchServoEcho)

        subprocess.call(e0, shell=True)
        time.sleep(0.01)
        subprocess.call(e1, shell=True)
        time.sleep(0.01)
        subprocess.call(e2, shell=True)
        time.sleep(0.01)
        subprocess.call(rotServoEcho, shell=True)
        time.sleep(0.01)
        subprocess.call(pitchServoEcho, shell=True)
        time.sleep(0.01)

if __name__ == "__main__":
    joy = XboxToBotRoss()
    checked_time = time.time()
    while True:
        values = joy.read()
        # print(values)
        if time.time() > checked_time + 0.1:
            joy.processToSerial(values)
            checked_time = time.time()
