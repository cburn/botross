#include <config.h>
#include <scheduler.h>
#include <translator.h>

Scheduler scheduler;
Translator translator;

Servo pitchServo;
Servo rollServo;
bool toggle;
unsigned long lastTimeChecked;
/* Translator translator; */
void setup() {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);
    delay(100);
    digitalWrite(LED_BUILTIN, LOW);
    delay(100);
    digitalWrite(LED_BUILTIN, HIGH);
    delay(100);
    digitalWrite(LED_BUILTIN, LOW);
    delay(100);
    digitalWrite(LED_BUILTIN, HIGH);
    delay(100);
    digitalWrite(LED_BUILTIN, LOW);
    delay(100);
    const myServoStruct configuredServos [] = {{pitchServo, PITCH_SERVO},{rollServo, ROLL_SERVO}};

    for (int i = 0; i < sizeof(outputPins) / sizeof(outputPins[0]); i++){
        pinMode(outputPins[i], OUTPUT);
    }

    for (int i = 0; i < sizeof(configuredServos) / sizeof(configuredServos[0]); i++){
        configuredServos[i].servo.attach(configuredServos[i].pin);
    }

    Serial.begin(BAUD_RATE);
    Serial.print("in setup ");
    Serial.println((int)&scheduler, HEX);
    scheduler.setup(&pitchServo, &rollServo);
    translator.setup(&scheduler);
    bool toggle = false;
    lastTimeChecked = micros();
}
void fastheartbeat () {
    toggle = !toggle;
    if(toggle){
        digitalWrite(LED_BUILTIN, HIGH);
    }
    else {
        digitalWrite(LED_BUILTIN, LOW);
    }
    lastTimeChecked = micros();
    /* Serial.println("fast heartbeat"); */
}
void heartbeat() {
    if(micros()-lastTimeChecked > 1000000){
        toggle = !toggle;
        if(toggle){
            digitalWrite(LED_BUILTIN, HIGH);
        }
        else {
            digitalWrite(LED_BUILTIN, LOW);
        }
        lastTimeChecked = micros();
        Serial.println("heartbeat");
    }
}

void loop() {
    /* Serial.println("top of loop"); */
    translator.readSerial();
    scheduler.runStep();
    heartbeat();
}
