#!/bin/bash

echo "R 0 100 10000">/dev/ttyACM0
sleep 0.001
echo "R 1 100 10000">/dev/ttyACM0

sleep 2

echo "R 0 100 10000">/dev/ttyACM0
sleep 0.001
echo "R 1 100 -10000">/dev/ttyACM0

sleep 2

echo "R 0 100 -10000">/dev/ttyACM0
sleep 0.001
echo "R 1 100 -10000">/dev/ttyACM0

sleep 2

echo "R 0 100 -10000">/dev/ttyACM0
sleep 0.001
echo "R 1 100 10000">/dev/ttyACM0

