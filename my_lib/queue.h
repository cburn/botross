#ifndef QUEUE
#define QUEUE
#include <Arduino.h>
#include <config.h>
#include <Servo.h>
struct StepperQueue {
    unsigned long timingStep [QUEUE_LEN];
    unsigned long timingPol [QUEUE_LEN];
    unsigned long lastTimeStepped;
    int currentIndex;
    int lastIndex;
    int steps;
    int stepPin;
    int enablePin;
    int dirPin;
    int numSteps;

    char command [MAX_CMD_LEN];
};
struct ServoQueue {
    // Delay for next adjustment
    unsigned long timingStep [QUEUE_LEN];
    // + or - what the new position should be
    char position [QUEUE_LEN];
    unsigned long lastTimeStepped;
    int currentIndex;
    int lastIndex;
    // where it currently is.
    int numSteps;
    int deltaSteps;
    char command [MAX_CMD_LEN];
    // what to drive to that position at that time.
    Servo *servo;
};
struct CommandQueue {
    char commandQueue [10][MAX_CMD_LEN];
};

#endif
