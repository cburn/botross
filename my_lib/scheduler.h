#ifndef SCHEDULER
#define SCHEDULER
#include <Arduino.h>
#include <queue.h>
#include <Servo.h>

class Scheduler{
    public:
        Scheduler();
        void runStep();
        void setup(Servo *pitch, Servo *roll);
        void setupQueue(StepperQueue *q);
        void setupQueue(ServoQueue *q);
        void processQueue(StepperQueue *q);
        void processQueue(ServoQueue *q);
        void stepMotor(StepperQueue *q);
        void stepMotor(ServoQueue *q);
        void updateQueue(int axis, int stepNums, long delayNums, char command []);
    private:
        StepperQueue stepperQueues [3];
        ServoQueue servoQueues[2];

};
#endif
