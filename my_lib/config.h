#ifndef CONFIG
#define CONFIG
#include <Servo.h>

struct myServoStruct {
    Servo servo;
    int pin;
};

/* const float PI = 3.14159; */
#define X_STEP         54
#define X_DIR          55
#define X_ENABLE       38
#define X_MIN           3
#define X_MAX           2

#define Y_STEP         60
#define Y_DIR          61
#define Y_ENABLE       56
#define Y_MIN          14
#define Y_MAX          15

#define Z_STEP         46
#define Z_DIR          48
#define Z_ENABLE       62
#define Z_MIN          18
#define Z_MAX          19

#define E_STEP         26
#define E_DIR          28
#define E_ENABLE       24

#define SDPOWER            -1
#define SDSS               53
#define LED            13
#define PITCH_SERVO  5
#define ROLL_SERVO  4
#define BAUD_RATE 115200
#define MAX_CMD_LEN 64
#define QUEUE_LEN 25
#define NUM_STEPPERS 3
#define NUM_SERVOS 2
const int outputPins [] = {Z_STEP, Z_DIR, Z_ENABLE, Y_STEP, Y_DIR, Y_ENABLE, X_STEP, X_DIR, X_ENABLE};

const float microsteps = 1;
const float radiusPulley = 8;
const float stepPerRotation = 400;
const float mmPerStep = 2 * PI * radiusPulley / stepPerRotation;
#endif
