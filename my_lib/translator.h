#ifndef TRANSLATOR
#define TRANSLATOR
#include <Arduino.h>
#include <scheduler.h>

class Translator {
    public:
        Translator();
        void serialUpdateStatus();
        void readSerial();
        void setup(Scheduler *scheduler);
        void processCommand();
    private:
        bool commandReady;
        int readIndex;
        int axis;
        char commandChar [MAX_CMD_LEN];
        CommandQueue commandQueue;
        Scheduler *scheduler;
        char commandType [1];
        int stepNums;
        long delayNums;

};
#endif
