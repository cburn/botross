#include <Arduino.h>
#include "scheduler.h"
#include <Servo.h>

Scheduler::Scheduler(){
}

void Scheduler::setup(Servo *pitch, Servo *roll){
    for (int i = 0; i < NUM_STEPPERS; i++) {
        setupQueue(&stepperQueues[i]);
        Serial.print("Starting stepper axis ");
        Serial.print(i);
        Serial.print(" Queue currentIn ");
        Serial.println(stepperQueues[i].currentIndex);
        delay(500);
    }
    for (int i = 0; i < NUM_SERVOS; i++) {
        setupQueue(&servoQueues[i]);
        Serial.print("Starting servo axis ");
        Serial.print(i);
        Serial.print(" Queue currentIn ");
        Serial.println(servoQueues[i].currentIndex);
        delay(500);
    }

    stepperQueues[0].stepPin = 54;
    stepperQueues[0].dirPin = 55;
    stepperQueues[0].enablePin = 38;
    stepperQueues[1].stepPin = 60;
    stepperQueues[1].dirPin = 61;
    stepperQueues[1].enablePin = 56;
    stepperQueues[2].stepPin = 46;
    stepperQueues[2].dirPin = 48;
    stepperQueues[2].enablePin = 62;
    digitalWrite(38, LOW);
    digitalWrite(56, LOW);
    digitalWrite(62, LOW);
    servoQueues[0].servo = roll;
    servoQueues[1].servo = pitch;
    delay(500);

}

void Scheduler::runStep(){
    int i = 0;
    for (i = 0; i < NUM_STEPPERS; i++) {
        processQueue(&stepperQueues[i]);
    }
    for (i = 0; i < NUM_SERVOS; i++) {
        processQueue(&servoQueues[i]);
    }
    for (i = 0; i < NUM_STEPPERS; i++){
        digitalWrite(stepperQueues[i].stepPin, LOW);
    }
}

void Scheduler::processQueue(ServoQueue *q){
    unsigned long i = q->currentIndex;
    unsigned long j = q->lastIndex;
    if (j==i){
        return;
    }
    unsigned long lt = q->lastTimeStepped;
    unsigned long n = micros();
    char commandType = q->command[0];
    /* Serial.print(commandType); */
    if (commandType == 'R'){
        if(q->timingStep[q->currentIndex] + q->lastTimeStepped < n && abs(q->numSteps) > 0){
            stepMotor(q);
            q->lastTimeStepped = n;
        }
        if(0 == q->numSteps){
            q->lastIndex = q->currentIndex;
        }
    }
}

void Scheduler::processQueue(StepperQueue *q) {
    unsigned long i = q->currentIndex;
    unsigned long j = q->lastIndex;
    if (j==i){
        return;
    }
    /* Serial.println(i); */
    /* Serial.println(j); */
    /* Serial.println(q->numSteps); */
    unsigned long lt = q->lastTimeStepped;
    unsigned long n = micros();
    char commandType = q->command[0];

    if (commandType == 'R'){
        /* Serial.println("In command R"); */
        if(q->timingStep[q->currentIndex] + q->lastTimeStepped < n && q->numSteps > 0){
            stepMotor(q);
            q->numSteps--;
            q->lastTimeStepped = n;
        }
        if(0 == q->numSteps){
            q->lastIndex = q->currentIndex;
        }
    }
}

void Scheduler::stepMotor(StepperQueue *q){
    /* digitalWrite(q->dirPin, q->timingPol[q->currentIndex]); */
    digitalWrite(q->stepPin, HIGH);
    /* Serial.print("stepping "); */
    /* Serial.println(q->stepPin); */
}

void Scheduler::stepMotor(ServoQueue *q){
    Servo *servo = q->servo;
    q->numSteps = constrain(q->numSteps + q->deltaSteps, 25, 165);
    servo->write(q->numSteps);
    /* digitalWrite(q->dirPin, q->timingPol[q->currentIndex]); */
    /* digitalWrite(q->stepPin, HIGH); */
    /* Serial.print("stepping "); */
    /* Serial.println(q->stepPin); */
}

void Scheduler::setupQueue(StepperQueue *q){
    for (int i = 0; i < QUEUE_LEN ; i++){
        q->timingStep[i] = 0;
    }
    q->lastTimeStepped = micros();
    q->currentIndex = 0;
    q->lastIndex = 0;
    q->steps = 0;
    q->command[0] = '\0';
    q->numSteps = 0;
}

void Scheduler::setupQueue(ServoQueue *q){
    for (int i = 0; i < QUEUE_LEN ; i++){
        q->timingStep[i] = 0;
    }
    q->lastTimeStepped = micros();
    q->currentIndex = 0;
    q->lastIndex = 0;
    q->numSteps = 90;
    q->command[0] = '\0';
    q->deltaSteps = 0;
    Servo *servo = q->servo;
    servo->write(q->numSteps);
}

void Scheduler::updateQueue(int axis, int stepNum, long delayNum, char command []){
    StepperQueue *q;
    ServoQueue *sq;

    /* Serial.println(axis); */
    switch(axis){
        case 0:
            /* Serial.println("updating axis 0"); */
            q = &stepperQueues[0];
            q->numSteps = abs(stepNum);
            if(delayNum > 0){
                digitalWrite(q->dirPin, HIGH);
            }
            else {
                digitalWrite(q->dirPin, LOW);
            }
            q->timingStep[q->currentIndex] = abs(delayNum);
            q->lastIndex = q->currentIndex + 1;
            memcpy(q->command, command, MAX_CMD_LEN);
            break;
        case 1:
            /* Serial.println("updating axis 1"); */
            q = &stepperQueues[1];
            q->numSteps = abs(stepNum);
            if(delayNum > 0){
                digitalWrite(q->dirPin, HIGH);
            }
            else {
                digitalWrite(q->dirPin, LOW);
            }
            q->timingStep[q->currentIndex] = abs(delayNum);
            q->lastIndex = q->currentIndex + 1;
            memcpy(q->command, command, MAX_CMD_LEN);
            break;
        case 2:
            /* Serial.println("updating axis 2"); */
            q = &stepperQueues[2];
            q->numSteps = abs(stepNum);
            if(delayNum > 0){
                digitalWrite(q->dirPin, HIGH);
            }
            else {
                digitalWrite(q->dirPin, LOW);
            }
            q->timingStep[q->currentIndex] = abs(delayNum);
            q->lastIndex = q->currentIndex + 1;
            memcpy(q->command, command, MAX_CMD_LEN);
            break;
        case 3:
            sq = &servoQueues[0];
            sq->deltaSteps = 0;
            if (delayNum > 0) {
                sq->deltaSteps = 1;
            }
            else if (delayNum < 0) {
                sq->deltaSteps = -1;
            }
            sq->timingStep[sq->currentIndex] = abs(delayNum);
            sq->lastIndex = sq->currentIndex + 1;
            memcpy(sq->command, command, MAX_CMD_LEN);
            break;
        case 4:
            sq = &servoQueues[1];
            sq->deltaSteps = 0;
            if (delayNum > 0) {
                sq->deltaSteps = 1;
            }
            else if (delayNum < 0) {
                sq->deltaSteps = -1;
            }
            sq->timingStep[sq->currentIndex] = abs(delayNum);
            sq->lastIndex = sq->currentIndex + 1;
            memcpy(sq->command, command, MAX_CMD_LEN);
            break;
    }
}
