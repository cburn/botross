#include <Arduino.h>
#include "translator.h"


Translator::Translator(){

}
void Translator::setup(Scheduler *sch) {
    scheduler = sch;
    commandReady = true;
    readIndex = 0;

}
void Translator::serialUpdateStatus() {
    Serial.println("in status");
}

void Translator::readSerial() {
    if(!Serial.available()) return;
    if(commandReady){
        readIndex = 0;
    }
    commandReady = false;
    while(Serial.available() && ! commandReady){
        commandChar[readIndex] = Serial.read();
        readIndex++;
        if (commandChar[readIndex-1] == '\n'){
            commandChar[readIndex] = '\0';
            commandReady = true;
        }
    }
    if (commandReady) {
        processCommand();
    }

}

void Translator::processCommand() {
    /* Serial.println(commandChar); */
    sscanf(commandChar, "%s %d %d %ld", commandType, &axis, &stepNums, &delayNums);
    /* char* resstr = strchr( commandChar, ',' ) + 1 ; */
    /* axis = atoi( resstr ) ; */
    /*  */
    /* resstr = strchr( resstr, ',' ) + 1 ; */
    /* stepNums = atoi( resstr ) ; */
    /*  */
    /* resstr = strchr( resstr, ',' ) + 1 ; */
    /* char *ptr; */
    /* delayNums = strtoul( resstr, &ptr, 10 ) ; */
    /* Serial.println("proc command"); */
    /* Serial.println(axis); */
    /* Serial.println(stepNums); */
    /* Serial.println(delayNums); */
    scheduler->updateQueue(axis, stepNums, delayNums, commandChar);
}
